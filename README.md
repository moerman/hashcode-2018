hashcode-2018
=============

```
             ,----------------,              ,---------,
        ,-----------------------,          ,"        ,"|
      ,"                      ,"|        ,"        ,"  |
     +-----------------------+  |      ,"        ,"    |
     |  .-----------------.  |  |     +---------+      |
     |  | Google Hashcode |  |  |     | -==----'|      |
     |  | 2018 met Jeroen |  |  |/----|`---=    |      |
     |  | Joshua Julian & |  |  |   ,/|==== ooo |      ;
     |  |     Vincent     |  |  |  // |(((( [33]|    ,"
     |  `-----------------'  |," .;'| |((((     |  ,"   Ascii art by
     +-----------------------+  ;;  | |         |,"     -Kevin  Lam-
        /_)______________(_/  //'   | +---------+
   ___________________________/___  `,
  /  oooooooooooooooo  .o.  oooo /,   \,"-----------
 / ==ooooooooooooooo==.o.  ooo= //   ,`\--{)B     ,"
/_==__==========__==_ooo__ooo=_/'   /___________,"
`-----------------------------'
```
