#include <algorithm>
#include <random>
#include <fstream>

#include "input.hpp"
#include "julian.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	string output_name = "no_name";
	if (argc == 2) {
		output_name = argv[1];
	}

	auto in = read_input();

	mt19937 gen;
	uniform_int_distribution<int> dist(0, in.F - 1);

	int64_t best = 0;
	while (true) {
		shuffle(in.rides.begin(), in.rides.end(), gen);
		vector<vector<Ride>> retarded(in.F);

		for (const auto r : in.rides) {
			retarded[dist(gen)].push_back(r);
		}

		const auto score = eval(retarded, in.B);
		if (score > best) {
			best = score;
			clog << "better: " << score << endl;

			ofstream fout(output_name + to_string(score) + ".out");
			print_output(retarded, fout);
		}
	}
}
