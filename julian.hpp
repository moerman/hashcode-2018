// Copyright 2018 ReSnap b.v. All Rights Reserved.

#ifndef JULIAN_HPP
#define JULIAN_HPP

#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

#include "input.hpp"

using namespace std;

int64_t dist(const Point &from, const Point &to);

int64_t eval_rider(const vector<Ride> &rides, const int64_t B);
int64_t eval(const vector<vector<Ride>> &rides, const int64_t B);

vector<int64_t> first_n_numbers(const int64_t n);
vector<int64_t> generate_submatrix(const vector<vector<Ride>> &rides,
                                   const int64_t rows);
vector<int64_t> generate_weighted_submatrix(const vector<vector<Ride>> &rides,
                                            int64_t rows, const int64_t B);

vector<vector<Ride>> get_submatrix(const vector<vector<Ride>> &rides,
                                   const vector<int64_t> &rows);

vector<Ride> unplan_rider(const vector<Ride> &rides, vector<Ride> &pool);
void unplan(vector<vector<Ride>> &rides, vector<Ride> &pool);
void unplan_all(vector<vector<Ride>> &rides, vector<Ride> &pool);

void merge(vector<vector<Ride>> &rides, const vector<vector<Ride>> &sub,
           const vector<int64_t> &rows);

vector<Ride> reschedule_sim(vector<Ride> rider);

#endif // JULIAN_HPP
