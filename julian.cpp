// Copyright 2018 ReSnap b.v. All Rights Reserved.

#include "julian.hpp"

using namespace std;

int64_t dist(const Point &from, const Point &to) {
    return abs(from.x - to.x) + abs(from.y - to.y);
}

int64_t eval_rider(const vector<Ride> &rides, const int64_t B) {
    Point loc{0, 0};
    int64_t clock = 0, score = 0;
    for (const Ride &ride : rides) {
        clock = max(ride.earliest, clock + dist(loc, ride.start));
        if (clock + dist(ride.start, ride.finish) <= ride.latest) {
            score +=
                dist(ride.start, ride.finish) + B * (clock == ride.earliest);
        }
        clock += dist(ride.start, ride.finish);
        loc = ride.finish;
    }
    return score;
}

int64_t eval(const vector<vector<Ride>> &rides, const int64_t B) {
    int64_t total = 0;
    for (const auto &rider : rides) {
        total += eval_rider(rider, B);
    }
    return total;
}

vector<int64_t> first_n_numbers(const int64_t n) {
    vector<int64_t> res;
    for (int64_t i = 0; i < n; i++) {
        res.push_back(i);
    }
    return res;
}

vector<int64_t> generate_submatrix(const vector<vector<Ride>> &rides,
                                   const int64_t rows) {
    mt19937 gen;
    vector<int64_t> indices = first_n_numbers(rides.size());
    shuffle(indices.begin(), indices.end(), gen);
    vector<int64_t> sub(indices.begin(), indices.begin() + rows);
    return sub;
}

vector<int64_t> generate_weighted_submatrix(const vector<vector<Ride>> &rides,
                                            int64_t rows, const int64_t B) {
    vector<pair<int64_t, int64_t>> row_scores;
    int64_t total = 0;
    for (int64_t i = 0; i < rides.size(); i++) {
        auto &rider = rides[i];
        int64_t curr = eval_rider(rider, B);
        row_scores.push_back({curr, i});
        total += curr;
    }

    sort(row_scores.begin(), row_scores.end());
    vector<int64_t> sub;
    for (int64_t i = 0; rows > 0 && i < row_scores.size(); i++) {
        if (rand() % total > row_scores[i].first) {
            sub.push_back(row_scores[i].second);
            rows--;
        }
    }
    return sub;
}

vector<vector<Ride>> get_submatrix(const vector<vector<Ride>> &rides,
                                   const vector<int64_t> &rows) {
    vector<vector<Ride>> sub;
    for (const int64_t row : rows) {
        sub.push_back(rides[row]);
    }
    return sub;
}

vector<Ride> unplan_rider(const vector<Ride> &rides, vector<Ride> &pool) {
    vector<Ride> good;
    Point loc{0, 0};
    int64_t clock = 0;
    for (const Ride &ride : rides) {
        clock = max(ride.earliest, clock + dist(loc, ride.start));
        if (clock == ride.earliest &&
            clock + dist(ride.start, ride.finish) <= ride.latest) {
            good.push_back(ride);
        } else {
            pool.push_back(ride);
        }
        clock += dist(ride.start, ride.finish);
        loc = ride.finish;
    }
    return good;
}

vector<Ride> reschedule_sim(vector<Ride> rider) {
    vector<Ride> res, trash;
    sort(rider.begin(), rider.end(), [](const Ride &l, const Ride &r) {
        if (l.earliest == r.earliest)
            return dist(l.start, l.finish) > dist(r.start, r.finish);
        return l.earliest < r.earliest;
    });
    int64_t clock = 0;
    Point loc{0, 0};
    for (const auto &ride : rider) {
        if (clock + dist(loc, ride.start) <=
            ride.latest - dist(ride.start, ride.finish)) {
            res.push_back(ride);
            clock = max(ride.earliest, clock + dist(loc, ride.start)) +
                    dist(ride.start, ride.finish);
            loc = ride.finish;
        } else {
            trash.push_back(ride);
        }
    }
    res.insert(res.end(), trash.begin(), trash.end());
    return res;
}

void unplan(vector<vector<Ride>> &rides, vector<Ride> &pool) {
    for (auto &rider : rides) {
        rider = unplan_rider(rider, pool);
    }
}

void unplan_all(vector<vector<Ride>> &rides, vector<Ride> &pool) {
    for (auto &rider : rides) {
        for (const auto &ride : rider) {
            pool.push_back(ride);
        }
        rider.clear();
    }
}

void merge(vector<vector<Ride>> &rides, const vector<vector<Ride>> &sub,
           const vector<int64_t> &rows) {
    for (int64_t i = 0; i < rows.size(); i++) {
        rides[rows[i]] = sub[i];
    }
}

// int main() { return 0; }
