#pragma once

#include <vector>
#include <iostream>

struct Point {
	int64_t x, y;
};

struct Ride {
    Point start, finish;
    int64_t earliest, latest;
    int64_t id;
};

struct Input {
	int64_t R, C, F, N, B, T;
	std::vector<Ride> rides;
};

inline std::ostream& operator<<(std::ostream& out, Point p) {
	return out << '[' << p.x << ", " << p.y << ']';
}

inline std::ostream& operator<<(std::ostream& out, Ride r) {
	return out << '(' << r.id << ") " << r.start << " " << r.finish << " " << r.earliest << ' ' << r.latest;
}

inline std::ostream& operator<<(std::ostream& out, Input in) {
	using namespace std;
	out << in.R << ' ' << in.C << ' ' << in.F << ' ' << in.N << ' ' << in.B << ' ' << in.T << endl;
	for (int i = 0; i < in.N; ++i) {
		out << in.rides[i] << endl;
	}
	return out;
}

inline Input read_input() {
	using namespace std;

	Input in;
	cin >> in.R >> in.C >> in.F >> in.N >> in.B >> in.T;
	for (int i = 0; i < in.N; ++i) {
		Ride r;
		cin >> r.start.x >> r.start.y >> r.finish.x >> r.finish.y >> r.earliest >> r.latest;
		r.id = i;
		in.rides.push_back(r);
	}

	return in;
}

inline void print_output(std::vector<std::vector<Ride>> const & rides, std::ostream& out) {
	for (auto const v : rides) {
		out << v.size();
		for (auto const r : v) {
			out << ' ' << r.id;
		}
		out << std::endl;
	}
}
