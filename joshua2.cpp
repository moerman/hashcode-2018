#include <algorithm>
#include <random>
#include <fstream>

#include "input.hpp"
#include "julian.hpp"

using namespace std;

mt19937 gen;
uniform_int_distribution<> coin(0, 1);

struct Parameters {
	int sub_iterations = 1000;
	int sub_size_min = 2;
	int sub_size_max = 10;
	int rescheduleshuffle = 100;
} parameters;

int sort1_better = 0;
int sort2_better = 0;
int juvin_better = 0;

void plan(vector<Ride> & pool, vector<vector<Ride>> & sub_puzzle) {
	uniform_int_distribution<int> dist(0, sub_puzzle.size() - 1);
	shuffle(pool.begin(), pool.end(), gen);

	for (const auto r : pool) {
		sub_puzzle[dist(gen)].push_back(r);
	}

	pool.clear();
}

void reschedule(vector<Ride> & pool, vector<vector<Ride>> & sub_puzzle, Input const & in) {
	for (auto & rider : sub_puzzle) {
		sort(rider.begin(), rider.end(), [](auto const & l, auto const & r) {
			if(l.earliest == r.earliest) return dist(l.start, l.finish) > dist(r.start, r.finish);
			return l.earliest < r.earliest;
		});
		auto score = eval({rider}, in.B);
		auto best = rider;
		int best_strat = 0;

		rider = reschedule_sim(rider);
		auto score3 = eval({rider}, in.B);
		if (score3 > score) {
			score = score3;
			best = rider;
			best_strat = 2;
		}

		if(best_strat == 0) sort1_better++;
		if(best_strat == 1) sort2_better++;
		if(best_strat == 2) juvin_better++;

		rider = best;
	}
}

int main(int argc, char* argv[]) {
	string output_name = "no_name";
	if (argc == 2) {
		output_name = argv[1];
	}

	auto in = read_input();

	uniform_int_distribution<int> distri(0, in.F - 1);
	uniform_int_distribution<int> subsize_dst(parameters.sub_size_min, parameters.sub_size_max);
	sort(in.rides.begin(), in.rides.end(), [](auto const & l, auto const & r) {
		if(l.earliest == r.earliest) return dist(l.start, l.finish) > dist(r.start, r.finish);
		return l.earliest < r.earliest;
	});

	vector<vector<Ride>> current_plan(in.F);
	vector<Ride> pool;

	// Plan everything randomly!
	for (const auto r : in.rides) {
		current_plan[distri(gen)].push_back(r);
	}

	int64_t best = eval(current_plan, in.B);
	while (true) {
		clog << ".";
		auto subpuz_rows = generate_weighted_submatrix(current_plan, subsize_dst(gen), in.B);
		auto subpuz = get_submatrix(current_plan, subpuz_rows);
		auto subpuz_kopie = subpuz;
		auto pool_kopie = pool;

		int64_t sub_best = 0;
		vector<vector<Ride>> sub_best_rides;
		for (int j = 0; j < parameters.sub_iterations; ++j) {
			if(coin(gen)){
				unplan_all(subpuz, pool);
			} else {
				unplan(subpuz, pool);
			}
			plan(pool, subpuz);
			reschedule(pool, subpuz, in);
			auto sub_score = eval(subpuz, in.B);
			if (sub_score > sub_best) {
				sub_best = sub_score;
				// TODO: we slaan de huidige pool niet op...
				sub_best_rides = subpuz;
			}
		}

		merge(current_plan, sub_best_rides, subpuz_rows);

		auto score = eval(current_plan, in.B);

		if (score > best) {
			best = score;

			clog << "better: " << score << endl;
			ofstream fout(output_name + to_string(score) + ".out");
			print_output(current_plan, fout);
		} else {
			// TODO: pool ook aanpassen!
			merge(current_plan, subpuz_kopie, subpuz_rows);
			pool = pool_kopie;
		}
	}
}
